export const state = () => ({
    countries: [],
    country: [],
});

export const actions = {
    // Call to get stats from all countries and a global overview
    async getAllStats({ commit }) {
        try {
            const response = await this.$axios.get("https://api.covid19api.com/summary");
            commit("saveCountriesStats", response.data.Countries);
        } catch (error) {
            console.log(error);
        }
    },

    async getCountryStats({ commit }, countryName) {
        try {
            const response = await this.$axios.get(`https://api.covid19api.com/dayone/country/${countryName}`);
            commit("saveCountryStats", response.data);
        } catch (error) {
            console.log(error);
        }
    }

}

export const mutations = {

    saveGlobalStats(state, global) {
        state.global = global;
    },

    saveCountriesStats(state , countries) {
        state.countries = countries;
    },

    saveCountryStats(state , country) {
        state.country = country;
    }
}
